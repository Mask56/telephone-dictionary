﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Telephone
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Decrypt("999337777"));
        }

        public static string Decrypt(string something)
        {
            string user_string = something;
            if (!string.IsNullOrEmpty(user_string) && Regex.IsMatch(user_string, "[0-9]+[' ']*"))
                //Проверяем имеет ли строка кроме чисел и пробелов еще что-нибудь
            {
                var map = new Dictionary<int, string>(); //Создаем словарь
                map.Add(0, " ");
                map.Add(1, "");
                map.Add(2, "abc");
                map.Add(3, "def");
                map.Add(4, "ghi");
                map.Add(5, "jkl");
                map.Add(6, "mno");
                map.Add(7, "pqrs");
                map.Add(8, "tuv");
                map.Add(9, "wxyz");
                StringBuilder telephone_text = new StringBuilder();
                int index = 0, repeat_numbers = 0, number_in_userstring = 0, number_in_newline = 0;
                while (user_string.Length > number_in_userstring)
                {
                    if (user_string[number_in_userstring] == ' ') //пропускаем пробелы
                        number_in_userstring++;

                    // Эта проверка нужна чтобы обезопасить пользователя если он на ключ с тремя буквами 
                    // нажмет цифру 4-е раза. В таком случае отсчет букв начнется заново
                    if (map[(int) Char.GetNumericValue(user_string[number_in_userstring])].Length > 2)
                        //Если пользователь попал на ключ с тремя буквами
                        while ((number_in_userstring + repeat_numbers + 2) <= user_string.Length &&
                               user_string[number_in_userstring] ==
                               user_string[number_in_userstring + repeat_numbers + 1])
                        {
                            if (index == 3)
                                index = 0;
                            index++;
                            repeat_numbers++;
                        }
                    else
                    {
                        while ((number_in_userstring + repeat_numbers + 2) <= user_string.Length &&
                               //Если пользователь попал на ключ с 4-мя буквами
                               user_string[number_in_userstring] ==
                               user_string[number_in_userstring + repeat_numbers + 1])
                        {
                            if (index == 4)
                                index = 0;
                            index++;
                            repeat_numbers++;
                        }
                    }

                    telephone_text.Insert(number_in_newline, //Добавляем друг за другом расшифрованные буквы
                        map[(int) Char.GetNumericValue(user_string[number_in_userstring])][index]);

                    number_in_userstring += repeat_numbers + 1; //Перепрыгиваем повторения чисел
                    index = 0; //Обнуляем индексы 
                    repeat_numbers = 0;
                    number_in_newline++;
                }


                return telephone_text.ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }
}
