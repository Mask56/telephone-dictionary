﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Telephone;

namespace Test_Decrypt
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Test_yes()
        {
            Assert.AreEqual("yes", Program.Decrypt("999337777"));

        }
        [TestMethod]
        public void Test_hi()
        {
            Assert.AreEqual("hi", Program.Decrypt("44 444"));

        }
        [TestMethod]
        public void Test_foo__bar()
        {
            Assert.AreEqual("foo  bar", Program.Decrypt("333666 6660 022 2777"));
        }
        [TestMethod]
        public void Test_hello_world()
        {
            Assert.AreEqual("hello world", Program.Decrypt("4433555 555666096667775553"));
        }
        [TestMethod]
        public void TestDecrypt_AreNotEqual()
        {
            Assert.AreNotEqual("hello world", Program.Decrypt("555 555666096667775553"));
        }
        [TestMethod]
        public void TestDecrypt_Null()
        {
            Assert.IsNull(null,Program.Decrypt("abcd"));
        }
    }
}
